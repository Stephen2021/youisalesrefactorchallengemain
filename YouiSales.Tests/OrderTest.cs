﻿using System;
using NUnit.Framework;
using Shouldly;
using YouiSales;

namespace YouiSales.Tests
{
    [TestFixture]


    public class OrderTest
    {

        #region Constructor

        private readonly static Policy BMW = new Policy("Jane Doe", "BMW", Policy.Car);
        private readonly static Policy Harley = new Policy("John Doe", "Harley", Policy.Motorcycle);
        private readonly static Policy SunnyCoast = new Policy("John Doe", "Sunshine Coast", Policy.Home);

        /// <summary>
        /// The newly created policies and prices have been implemented here.
        /// There are 4 new policies titled: Student, Senior, AdditionalPolicy1, and AdditionalPolicy2.
        /// Their prices have also been allocated within the seeded data.
        /// All 4 of the new policies have been seeded for both types of receipts.
        /// </summary>
        private readonly static Policy Student = new Policy("Jane Doe", "Student Loan", Policy.Student);
        private readonly static Policy Senior = new Policy("John Doe", "Senior Reward Scheme", Policy.Senior);
        private readonly static Policy AdditionalPolicy1 = new Policy("John Doe", "Policy Placeholder 1", Policy.AdditionalPolicy1);
        private readonly static Policy AdditionalPolicy2 = new Policy("John Doe", "Policy Placeholder 2", Policy.AdditionalPolicy2);

        #endregion

        #region Receipt One

        [Test]
        public void ReceiptOneBMW()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(BMW, 1));
            order.Receipt().ShouldBe(ResultStatementOneBMW);
        }

        private const string ResultStatementOneBMW = @"Order Receipt for Youi
	1 x Jane Doe BMW = $105.00
Sub-Total: $105.00
Tax: $10.50
Total: $115.50
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneHarley()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Harley, 1));
            order.Receipt().ShouldBe(ResultStatementOneHarley);
        }

        private const string ResultStatementOneHarley = @"Order Receipt for Youi
	1 x John Doe Harley = $56.00
Sub-Total: $56.00
Tax: $5.60
Total: $61.60
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneSunnyCoast()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(SunnyCoast, 1));
            order.Receipt().ShouldBe(ResultStatementOneSunnyCoast);
        }

        private const string ResultStatementOneSunnyCoast = @"Order Receipt for Youi
	1 x John Doe Sunshine Coast = $235.00
Sub-Total: $235.00
Tax: $23.50
Total: $258.50
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneStudent()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Student, 4));
            order.Receipt().ShouldBe(ResultStatementOneStudent);
        }

        private const string ResultStatementOneStudent = @"Order Receipt for Youi
	1 x Jane Doe Student Loan = $25.00
Sub-Total: $25.00
Tax: $2.50
Total: $27.50
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneSenior()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Senior, 5));
            order.Receipt().ShouldBe(ResultStatementOneSenior);
        }

        private const string ResultStatementOneSenior = @"Order Receipt for Youi
	1 x Jane Doe Student Loan = $70.00
Sub-Total: $70.00
Tax: $7.00
Total: $77.00
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneAdditionalPolicy1()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(AdditionalPolicy1, 6));
            order.Receipt().ShouldBe(ResultStatementOneAdditionalPolicy1);
        }

        private const string ResultStatementOneAdditionalPolicy1 = @"Order Receipt for Youi
	1 x Jane Doe Student Loan = $50.00
Sub-Total: $50.00
Tax: $5.00
Total: $55.00
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneAdditionalPolicy2()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(AdditionalPolicy2, 6));
            order.Receipt().ShouldBe(ResultStatementOneAdditionalPolicy2);
        }

        private const string ResultStatementOneAdditionalPolicy2 = @"Order Receipt for Youi
	1 x Jane Doe Student Loan = $50.00
Sub-Total: $50.00
Tax: $5.00
Total: $55.00
Date: Friday, 25 October 2019 9:07:27 AM";

        #endregion

        #region HTML Receipt

        [Test]
        public void HtmlReceiptOneBMW()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(BMW, 1));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneBMW);
        }

        private const string HtmlResultStatementOneBMW = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x Jane Doe BMW = $105.00</li></ul><h3>Sub-Total: $105.00</h3><h3>Tax: $10.50</h3><h2>Total: $115.50</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneHarley()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Harley, 2));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneHarley);
        }

        private const string HtmlResultStatementOneHarley = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x John Doe Harley = $56.00</li></ul><h3>Sub-Total: $56.00</h3><h3>Tax: $5.60</h3><h2>Total: $61.60</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneSunnyCoast()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(SunnyCoast, 3));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneSunnyCoast);
        }

        private const string HtmlResultStatementOneSunnyCoast = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x John Doe Sunshine Coast = $235.00</li></ul><h3>Sub-Total: $235.00</h3><h3>Tax: $23.50</h3><h2>Total: $258.50</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneStudent()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Student, 4));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneStudent);
        }

        private const string HtmlResultStatementOneStudent = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x Jane Doe Student = $25.00</li></ul><h3>Sub-Total: $25.00</h3><h3>Tax: $2.50</h3><h2>Total: 27.50</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneSenior()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Senior, 5));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneSenior);
        }

        private const string HtmlResultStatementOneSenior = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x Jane Doe Student = 70.00</li></ul><h3>Sub-Total: 70.00</h3><h3>Tax: $7.00</h3><h2>Total: $77.00</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneAdditionalPolicy1()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(AdditionalPolicy1, 6));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneAdditionalPolicy1);
        }

        private const string HtmlResultStatementOneAdditionalPolicy1 = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x Jane Doe Student = $50.00</li></ul><h3>Sub-Total: $50.00</h3><h3>Tax: $5.00</h3><h2>Total: 55.00</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneAdditionalPolicy2()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(AdditionalPolicy2, 7));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneAdditionalPolicy2);
        }

        private const string HtmlResultStatementOneAdditionalPolicy2 = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x Jane Doe Student = $50.00</li></ul><h3>Sub-Total: $50.00</h3><h3>Tax: $5.00</h3><h2>Total: 55.00</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        #endregion

        #region Ticket Receipt

        /// <summary>
        /// This set of seeded data has been added to seed the additional receipt format
        /// Create an instance of the order
        /// Add lines to the order
        /// print out the seeded order
        /// </summary>
        [Test]
        public void TicketReceiptOneBMW()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(BMW, 8));
            order.TicketReceipt();
        }

        /// <summary>
        /// This set of seeded data has been added to seed the additional receipt format
        /// Create an instance of the order
        /// Add lines to the order
        /// print out the seeded order
        /// </summary>
        [Test]
        public void TicketReceiptOneHarley()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Harley, 9));
            order.TicketReceipt();
        }

        /// <summary>
        /// This set of seeded data has been added to seed the additional receipt format
        /// Create an instance of the order
        /// Add lines to the order
        /// print out the seeded order
        /// </summary>
        [Test]
        public void TicketReceiptOneSunnyCoast()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(SunnyCoast, 10));
            order.TicketReceipt();
        }

        /// <summary>
        /// This set of seeded data has been added to seed the additional receipt format
        /// Create an instance of the order
        /// Add lines to the order
        /// print out the seeded order
        /// </summary>
        [Test]
        public void TicketReceiptOneStudent()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Student, 11));
            order.TicketReceipt();
        }

        /// <summary>
        /// This set of seeded data has been added to seed the additional receipt format
        /// Create an instance of the order
        /// Add lines to the order
        /// print out the seeded order
        /// </summary>
        [Test]
        public void TicketReceiptOneSenior()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Senior, 12));
            order.TicketReceipt();
        }

        /// <summary>
        /// This set of seeded data has been added to seed the additional receipt format
        /// Create an instance of the order
        /// Add lines to the order
        /// print out the seeded order
        /// </summary>
        [Test]
        public void TicketReceiptOneAdditionalPolicy1()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(AdditionalPolicy1, 13));
            order.TicketReceipt();
        }

        /// <summary>
        /// This set of seeded data has been added to seed the additional receipt format
        /// Create an instance of the order
        /// Add lines to the order
        /// print out the seeded order
        /// </summary>
        [Test]
        public void TicketReceiptOneAdditionalPolicy2()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(AdditionalPolicy2, 14));
            order.TicketReceipt();
        }

        #endregion

    }
}
