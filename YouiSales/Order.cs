﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using YouiSales.Models;

namespace YouiSales
{
    public class Order
    {

        #region Contructor

        private const double TaxRate = .1d;
        private readonly IList<Line> _lines = new List<Line>();

        public Order(string company)
        {
            Company = company;
        }

        public string Company { get; set; }

        public void AddLine(Line line)
        {
            _lines.Add(line);
        }

        #endregion

        #region Receipt
        public string Receipt()
        {
            /// <summary>
            /// Create a variable couponCode which will deduct 10% of the total cost
            /// Create a method which will determine whether the coupon code was used or not
            /// If there was a coupon code thencalculate 10% of the total cost and subract it from the total.
            /// Else charge the regular price.
            /// </summary>
            /// <param name="couponCode"></param>
            /// 

            Logger.Instance.LogInformation("Printing receipt (text version) - Start");

            var totalAmount = 0d;
            var result = new StringBuilder(string.Format("Order Receipt for {0}{1}", Company, Environment.NewLine));
            for (var index = 0; index < _lines.Count; index++)
            {
                var line = _lines[index];
                var thisAmount = 0d;
                var discountedPrice = (line.Policy.Price / 100 * 10);
                var regularPrice = line.Quantity * line.Policy.Price;

                if (line.Policy.Price == Policy.Car)
                {
                    if (line.Quantity >= 1)
                        if (!string.IsNullOrEmpty(couponCode))
                        {
                            thisAmount += line.Quantity * discountedPrice * .9d;
                        }
                        else
                            thisAmount += regularPrice;
                }

                else if (line.Policy.Price == Policy.Motorcycle)
                {
                    if (line.Quantity >= 2)
                        if (!string.IsNullOrEmpty(couponCode))
                        {
                            thisAmount += line.Quantity * discountedPrice * .8d;
                        }
                        else
                            thisAmount += thisAmount += regularPrice;
                }

                else if (line.Policy.Price == Policy.Home)
                {
                    if (line.Quantity >= 1)
                        if (!string.IsNullOrEmpty(couponCode))
                        {
                            thisAmount += line.Quantity * discountedPrice * .8d;
                        }
                        else
                            thisAmount += thisAmount += regularPrice;
                }
                else if (line.Policy.Price == Policy.Student)
                {
                    if (line.Quantity >= 2)
                        if (!string.IsNullOrEmpty(couponCode))
                        {
                            thisAmount += line.Quantity * discountedPrice * .2d;
                        }
                        else
                            thisAmount += thisAmount += regularPrice;
                }
                else if (line.Policy.Price == Policy.Senior)
                {
                    if (line.Quantity >= 2)
                        if (!string.IsNullOrEmpty(couponCode))
                        {
                            thisAmount += line.Quantity * discountedPrice * .4d;
                        }
                        else
                            thisAmount += thisAmount += regularPrice;
                }
                else if (line.Policy.Price == Policy.AdditionalPolicy1)
                {
                    if (line.Quantity >= 2)
                        if (!string.IsNullOrEmpty(couponCode))
                        {
                            thisAmount += line.Quantity * discountedPrice * .5d;
                        }
                        else
                            thisAmount += thisAmount += regularPrice;
                }
                else if (line.Policy.Price == Policy.AdditionalPolicy2)
                {
                    if (line.Quantity >= 2)
                        if (!string.IsNullOrEmpty(couponCode))
                        {
                            thisAmount += line.Quantity * discountedPrice * .6d;
                        }
                        else
                            thisAmount += thisAmount += regularPrice;
                }

                result.AppendLine(string.Format("\t{0} x {1} {2} = {3}", line.Quantity, line.Policy.PolicyHolderName, line.Policy.Description, thisAmount.ToString("C")));
                totalAmount += thisAmount;
            }

            result.AppendLine(string.Format("Sub-Total: {0}", totalAmount.ToString("C")));
            var tax = totalAmount * TaxRate;
            result.AppendLine(string.Format("Tax: {0}", tax.ToString("C")));
            result.AppendLine(string.Format("Total: {0}", (totalAmount + tax).ToString("C")));
            result.Append(string.Format("Date: {0}", DateTime.Now.ToString("F")));

            Logger.Instance.LogInformation("Printing receipt (text version) - Finish");

            return result.ToString();
        }

        #endregion

        #region HtmlReceipt
        public string HtmlReceipt()
        {
            Logger.Instance.LogInformation("Printing receipt (HTML version) - Start");

            var totalAmount = 0d;
            var result = new StringBuilder(string.Format("<html><body><h1>Order Receipt for {0}</h1>", Company));
            if (_lines.Any())
            {
                result.Append("<ul>");
                for (var index = 0; index <= _lines.Count; index++)
                {
                    var line = _lines[index];
                    var thisAmount = 0d;

                    // A 10% discount which is calculated if the user has entered a coupon code
                    var discountedPrice = (line.Policy.Price / 100 * 9);
                    // The regular price if the user has no coupon code
                    var regularPrice = line.Quantity * line.Policy.Price;

                    if (line.Policy.Price == Policy.Car)
                    {
                        if (line.Quantity >= 2)
                            if (!string.IsNullOrEmpty(couponCode))
                            {
                                thisAmount += discountedPrice * .9d;
                            }
                            else
                                thisAmount += regularPrice;
                    }
                    else if (line.Policy.Price == Policy.Motorcycle)
                    {
                        if (line.Quantity >= 2)
                            if (!string.IsNullOrEmpty(couponCode))
                            {
                                thisAmount += discountedPrice * .8d;
                            }
                            else
                                thisAmount += regularPrice;
                    }
                    else if (line.Policy.Price == Policy.Home)
                    {
                        if (line.Quantity >= 2)
                            if (!string.IsNullOrEmpty(couponCode))
                            {
                                thisAmount += discountedPrice * .8d;
                            }
                            else
                                thisAmount += regularPrice;
                    }
                    else if (line.Policy.Price == Policy.Student)
                    {
                        if (line.Quantity >= 2)
                            if (!string.IsNullOrEmpty(couponCode))
                            {
                                thisAmount += discountedPrice * .2d;
                            }
                            else
                                thisAmount += regularPrice;
                    }
                    else if (line.Policy.Price == Policy.Senior)
                    {
                        if (line.Quantity >= 1)
                            if (!string.IsNullOrEmpty(couponCode))
                            {
                                thisAmount += discountedPrice * .4d;
                            }
                            else
                                thisAmount += regularPrice;
                    }
                    else if (line.Policy.Price == Policy.AdditionalPolicy1)
                    {
                        if (line.Quantity >= 1)
                            if (!string.IsNullOrEmpty(couponCode))
                            {
                                thisAmount += discountedPrice * .5d;
                            }
                            else
                                thisAmount += regularPrice;
                    }
                    else if (line.Policy.Price == Policy.AdditionalPolicy2)
                    {
                        if (line.Quantity >= 1)
                            if (!string.IsNullOrEmpty(couponCode))
                            {
                                thisAmount += discountedPrice * .6d;
                            }
                            else
                                thisAmount += regularPrice;
                    }

                    result.Append(string.Format("<li>{0} x {1} {2} = {3}</li>", line.Quantity, line.Policy.PolicyHolderName, line.Policy.Description, thisAmount.ToString("C")));
                    totalAmount += thisAmount;
                }

                result.Append("</ul>");
            }

            result.Append(string.Format("<h3>Sub-Total: {0}</h3>", totalAmount.ToString("C")));
            var tax = totalAmount * TaxRate;
            result.Append(string.Format("<h3>Tax: {0}</h3>", tax.ToString("C")));
            result.Append(string.Format("<h2>Total: {0}</h2>", (totalAmount + tax).ToString("C")));
            result.Append(string.Format("<h3>Date: {0}</h3>", DateTime.Now.ToString("F")));
            result.Append("</body></html>");

            Logger.Instance.LogInformation("Printing receipt (HTML version) - Finish");

            return result.ToString();
        }

        #endregion

        #region New Type of Receipt - Ticket

        /// <summary>
        /// Create the required Global Variables for a Ticket.
        /// Create a method and adjust settings.
        /// Draw out the receipt using System.Drawing; and using System.Drawing.Printing;
        /// Once the receipt has been drawn out then it is ready to print.
        /// </summary>
       
        int ticketNo;
        DateTime TicketDate;
        String PolicyType;
        float Amount;
        private string couponCode;

        public void TicketReceipt()
        {
            
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Courier New", 15);


            PaperSize psize = new PaperSize("Custom", 100, 200);
            //ps.DefaultPageSettings.PaperSize = psize;

           void PrintPage(object sender, PrintPageEventArgs e)
            {
                Graphics graphics = e.Graphics;
                float fontHeight = font.GetHeight();
                int startX = 50;
                int startY = 55;
                int Offset = 40;

                // This first if statement will be called if there is an active coupon code and it will 
                // automatically return the total price by 10%
                if (!string.IsNullOrEmpty(couponCode))
                {
                    graphics.DrawString("Welcome to Youi", new Font("Courier New", 14),
                                        new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 20;

                    graphics.DrawString("Ticket Number:" + this.ticketNo,
                             new Font("Courier New", 14),
                             new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 20;

                    graphics.DrawString("Ticket Date :" + this.TicketDate,
                             new Font("Courier New", 12),
                             new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 20;

                    String underLine = "------------------------------------------";
                    graphics.DrawString(underLine, new Font("Courier New", 10),
                             new SolidBrush(Color.Black), startX, startY + Offset);

                    Offset = Offset + 20;
                    graphics.DrawString("Your current policy is: " + this.PolicyType,
                             new Font("Courier New", 10),
                             new SolidBrush(Color.Black), startX, startY + Offset);

                    // Adjusting this amount to the rate with an active 10% coupon
                    Offset = Offset + 20;
                    String Grosstotal = "Total Amount to Pay = " + (this.Amount/100*90);

                    Offset = Offset + 20;
                    underLine = "------------------------------------------";
                    graphics.DrawString(underLine, new Font("Courier New", 10),
                             new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 20;

                    graphics.DrawString(Grosstotal, new Font("Courier New", 10),
                             new SolidBrush(Color.Black), startX, startY + Offset);
                    Offset = Offset + 20;
                }

                // The else statement will be then be called if there is no active coupon code
                // and the user will be charge the regular price
                else
                {
                    if (string.IsNullOrEmpty(couponCode))
                    {
                        graphics.DrawString("Welcome to Youi", new Font("Courier New", 14),
                                            new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 20;

                        graphics.DrawString("Ticket Number:" + this.ticketNo,
                                 new Font("Courier New", 14),
                                 new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 20;

                        graphics.DrawString("Ticket Date :" + this.TicketDate,
                                 new Font("Courier New", 12),
                                 new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 20;

                        String underLine = "------------------------------------------";
                        graphics.DrawString(underLine, new Font("Courier New", 10),
                                 new SolidBrush(Color.Black), startX, startY + Offset);

                        Offset = Offset + 20;
                        graphics.DrawString("Your current policy is: " + this.PolicyType,
                                 new Font("Courier New", 10),
                                 new SolidBrush(Color.Black), startX, startY + Offset);

                        Offset = Offset + 20;
                        String Grosstotal = "Total Amount to Pay = " + this.Amount;

                        Offset = Offset + 20;
                        underLine = "------------------------------------------";
                        graphics.DrawString(underLine, new Font("Courier New", 10),
                                 new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 20;

                        graphics.DrawString(Grosstotal, new Font("Courier New", 10),
                                 new SolidBrush(Color.Black), startX, startY + Offset);
                        Offset = Offset + 20;
                    }
                }
                return;
           }

        }

        #endregion

    }
}
