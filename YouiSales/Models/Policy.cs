﻿using System;

namespace YouiSales
{
    public class Policy
    {
        /// <summary>
        /// Instantiating each type of policy and its respective cost
        /// </summary>
        public const int Car = 105;
        public const int Motorcycle = 56;
        public const int Home = 235;
        public const int Student = 25;
        public const int Senior = 70;
        public const int AdditionalPolicy1 = 50;
        public const int AdditionalPolicy2 = 50;

        // The required parameters for each Policy
        public Policy(string policyHolderName, string description, int price)
        {
            PolicyHolderName = policyHolderName;
            Description = description;
            Price = price;
        }

        // Creating a model for a Policy
        public string PolicyHolderName { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }

    }
}
