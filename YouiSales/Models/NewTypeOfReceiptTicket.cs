﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouiSales.Models
{
    /// <summary>
    /// This class has been created as an additional type of receipt format.
    /// The variables are firstly created
    /// The model for each category is created (TicketNo, ticketDate, policy, amount)
    /// An empty ticket is called and then populated with the values of the model.
    /// The variables- are called and assigned to the corresponding values.
    /// The ticket is now complete and is ready to be printed.
    /// </summary>
    /// 
    class NewTypeOfReceiptTicket
    {
        int ticketNo;
        DateTime TicketDate;
        String PolicyType;
        float Amount;

        public int TicketNo
        {
            //set the ticket number
            set { this.ticketNo = value; }
            //get the ticket number
            get { return this.ticketNo; }
        }
        public DateTime ticketDate
        {
            //set the current date
            set { this.TicketDate = value; }
            //get the current date
            get { return this.TicketDate; }
        }

        public String policy
        {
            //set the selected policy
            set { this.PolicyType = value; }
            //get the selected policy
            get { return this.PolicyType; }
        }
        public float amount
        {
            //set the corresponding amount
            set { this.Amount = value; }
            //get the corresponding amount
            get { return this.Amount; }
        }

        public NewTypeOfReceiptTicket()
        {

        }
        public NewTypeOfReceiptTicket(int ticketNo, DateTime TicketDate, String PolicyType, float Amount)
        {
            this.ticketNo = ticketNo;
            this.TicketDate = TicketDate;
            this.PolicyType = PolicyType;
            this.Amount = Amount;
        }
    }
}
