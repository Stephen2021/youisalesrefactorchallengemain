﻿using System;

public class Policy
{
	public Policy()
	{
        //Setting the required parameters for a Policy
        public Policy(string policyHolderName, string description, int price)
        {
            PolicyHolderName = policyHolderName;
            Description = description;
            Price = price;
        }

        //The model for a required policy
        public string PolicyHolderName { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
}
}
