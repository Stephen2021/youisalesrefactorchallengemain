# Coding Challenge

Thanks for taking the time to download this refactoring exercise. 

This solution contains three classes used by our imaginary sales department 
to produce order receipts and some unit tests to prove that everything works.

Pretend this code is part of a larger software system and one of the other 
developers on your team has submitted this code. 

Assume that this solution will change regularly as a result of fast moving 
product requirements. Aspects of the system that are likely to change include: 

 - More policies at new prices.
 - Different discount codes and percentages.
 - Additional receipt formats.

As a developer in the team:

- Prepare a code review document listing the points that you would like to 
address with the developer.
- Refactor the code so that it can survive an onslaught of change, you're 
confident it works, and you're comfortable the next engineer will easily 
understand how to work on it.
- Create a pull request for your changes.

*Hint: if there are multiple commits we will read each of them to gain insight 
into how you went about tackling the refactoring*

If we love your refactoring and your resume is legit, we'll move to the next
 step of the recruitment process.

**Show us what you can do! You should be proud of what you submit.**

# Skills Matrix

Please also rate yourself on the following scale for the skills below. 
This will help us align your skills with the roles available in different teams.


|Level |Description |
|--|--|
|0 |No knowledge or capability |
|1 |Basic level of understanding |
|2 |Basic level of Application |
|3 |Comprehensive level of Application |
|4 |Advanced level of Application |


|Skill |Level| Notes|
|--|--|--|
|**Server**|4|
| C# |4|
|.Net Ecosystem |4|
|WebAPI |4|
|ORM |0|
|NoSql |2|
|SQL |4|
|Unit Testing |3|
|Service Bus |0|
|**Browser** |2|
|SPA Frameworks |0|
|JavaScript |1|
|CSS pre processors |2|
|Unit testing |2|
|E2E UI testing |0|
|**Design Patterns** |1|
|OO/SOLID |1|
|CQRS |1|
|Domain Driven Design |1|
|Event Sourcing |1|
|Microservices and SOA |1|
|Distributed Systems |1|
|**Build / Deployment**
|Build Tools |4|
|CI/CD Tools |4|
|Deployment Strategies |4|
|Infrastructure as Code |4|
|Verson Control |4|
|**Cloud Platforms** |4|
|AWS |1|
|Azure |4|
|GCP or other |1|

